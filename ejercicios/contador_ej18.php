<?php

function abrirArchivo($path)
{
	if( !file_exists($path) )
	{
		echo 'El archivo no existe<br/>';
		return -1;
	}

	$manejador = fopen($path,"r+");

	if( !$manejador )
	{
		echo "El archivo no se pudo abrir por algún motivo<br/>";
		return -1;
	}

	return $manejador;
}

function imprimirVisitas()
{
	$path      = "contador.txt";
	$manejador = abrirArchivo($path);
	
	if( $manejador == -1 )
	{
		die("No se puede abrir el archivo, el script se debe terminar");
	}

	$cantidadVisitas = (integer)fgets($manejador);
	rewind($manejador);
	$cantidadVisitas += 1;
	fwrite($manejador, $cantidadVisitas);
	fclose($manejador);
	echo "Este Sitio recibio $cantidadVisitas visitas.";
}