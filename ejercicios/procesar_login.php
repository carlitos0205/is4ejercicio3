<?php
// Obtener los valores del formulario
$usuario = $_POST['usuario'];
$contrasena = $_POST['contrasena'];

// Leer el archivo de datos de usuarios
$archivoUsuarios = 'usuarios.txt';

if (file_exists($archivoUsuarios)) {
    $lineasUsuarios = file($archivoUsuarios, FILE_IGNORE_NEW_LINES);

    foreach ($lineasUsuarios as $linea) {
        list($usuarioGuardado, $contrasenaGuardada) = explode(':', $linea);
        if ($usuario === $usuarioGuardado && password_verify($contrasena, $contrasenaGuardada)) {
            // Las credenciales son válidas, redirigir al usuario a una página de bienvenida
            header("Location: bienvenida.php");
            exit;
        }
    }
}

// Si no se encontraron coincidencias, mostrar un mensaje de error
echo "Usuario o contraseña incorrectos. <a href='formulario.html'>Volver al inicio de sesión</a>";
?>
