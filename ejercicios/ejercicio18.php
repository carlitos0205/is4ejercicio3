<!--18-Ejercicio 18
Realizar un contador de visitas en PHP utilizando un archivo para mantener la cantidad de visitas a
la página.
Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio.-->
<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
    <title>Ejercicio 18</title>
    </head>
    <body>

    <?php
    require_once 'contador_ej18.php';
    imprimirVisitas();
    ?>
    
    </body>
</html>