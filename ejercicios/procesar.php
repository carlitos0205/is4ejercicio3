<!DOCTYPE html>
<html>
<head>
    <title>Resultado del Formulario</title>
</head>
<body>
    <h1>Resultado del Formulario</h1>
    <p>Los datos ingresados por el usuario son:</p>
    <?php
    // Recopilar datos del formulario
    $nombre = $_POST['nombre'];
    $apellido = $_POST['apellido'];
    $cedula = $_POST['cedula'];
    $hobby = $_POST['hobby'];
    $sexo = isset($_POST['sexo']) ? $_POST['sexo'] : '';
    $comentarios = $_POST['comentarios'];

    // Imprimir los datos en pantalla
    echo "<p>Nombre: $nombre</p>";
    echo "<p>Apellido: $apellido</p>";
    echo "<p>Cédula: $cedula</p>";
    echo "<p>Hobby: $hobby</p>";   
    echo "<p>Sexo: $sexo</p>";
    echo "<p>Comentarios:<br>$comentarios</p>";
    ?>
</body>
</html>
