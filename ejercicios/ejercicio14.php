<!--14- Ejercicio 14
Crear un array asociativo en PHP que cumpla los siguientes requisitos:
• Los índices/claves deben ser strings (estas cadenas deben ser generadas aleatoriamente
por el script y deben tener una longitud de 5 a 10 caracteres).
• Los valores deben ser números enteros del 1 a 1000.
• Se debe imprimir el vector completo indicando índice y valor.
• Se debe recorrer dicho array e imprimir sólo las claves del array que empiecen con la letra
a, d, m y z (función imprimir) (en el caso se no existir ningún índice que comience con esas
letras se debe imprimir un mensaje).
Observación: Se debe crear un archivo por función y el archivo principal donde se llaman a las
funciones y se realizan los procesamientos.-->
<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
    <title>Ejercicio 14</title>
    </head>
    <body>
    <?php
    require_once 'funciones_ej14.php';

    mt_srand(time());
    $vector = array();
    $vector = cargarArray($vector);
    echo "El vector es el siguiente<br/>";

    foreach ( $vector as $i => $v )
        echo $i. "=>". $v. "<br/>";

    echo "<br/><br/>";
    echo "El vector cuyos indices comienzan con: a,d,m,z<br/>";
    $vector = imprimirElementos($vector);
    foreach ( $vector as $i => $v )
        echo $i. "=>". $v. "<br/>";
    ?>
    
    </body>
</html>

