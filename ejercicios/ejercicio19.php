<!--19-Ejercicio 19
Dado un archivo con la siguiente información: una lista de matrículas, nombre, apellido y 3 notas
parciales para un grupo alumnos (el archivo puede tener un número variable de líneas de texto).
Procesar estos datos y construir un nuevo archivo que contenga solamente la matrícula y la
sumatoria de notas de los alumnos.-->
<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
    <title>Ejercicio 19</title>
    </head>
    <body>
    
    <?php
    require_once 'notas_ej19.php';

    $pathLectura   = "notas.txt";
    $pathEscritura = "sumaNotas.txt";
    cargarNotas($pathLectura,$pathEscritura);
    ?>
    
    </body>
</html>