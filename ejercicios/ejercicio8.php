<!-- 8- Ejercicio 8:
Hacer un script en PHP que genere una matriz de dimensión n*m con números aleatorios. Las
dimensiones n (filas) y m (columnas) son variables que debe definir el alumno. La matriz generada
se debe imprimir en pantalla de manera tabular.
Obsevación: El alumno deberá crear sus propias funciones para realizar este ejercicio. -->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 8</title>
    </head>
    <body>

    <?php
    const FILA    = 4;
    const COLUMNA = 5;

    mt_srand(time());

    $matriz = array();

    echo "<table border=1>";

    for( $i = 0; $i < FILA ; $i++ )
    {
        echo "<tr>";
        for( $j = 0; $j < COLUMNA; $j++ )
        {
            $matriz[$i][$j] = mt_rand(0,9);
            echo "<td>".$matriz[$i][$j]."</td>";
        }
        echo "</tr>";
    }
    echo "</table>";
    ?>

    </body>
</html>