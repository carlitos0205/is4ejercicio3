<!--7- Ejercicio 7:
Hacer un script en PHP que determine si una cadena dada es un palíndromo
Un palíndromo (del griego palin dromein, volver a ir hacia atrás) es una palabra, número o frase
que se lee igual hacia adelante que hacia atrás. Si se trata de un número, se llama capicúa.
Habitualmente, las frases palindrómicas se resienten en su significado cuanto más largas son
Ejemplos: radar, Neuquén, anilina, ananá, Malayalam
Obs: El alumno deberá crear sus propias funciones para realizar este ejercicio. -->
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ejercicio 7</title>
</head>
<body>

<?php
$cadenas = array("Rodador","Estrella","Colina","Ana","Anilina","Cerveza","Pizza");

foreach ($cadenas as $v)
{
	$v = strtolower($v);
	
	if($v == strrev($v))
		echo "La cadena $v es palindromo <br/>";
	else 
		echo "La cadena $v NO es palindromo <br/>";	
}
?>

</body>
</html>