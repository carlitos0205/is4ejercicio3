<!DOCTYPE html>
<html>
<head>
    <title>Formulario </title>
</head>
<body>
    <h1>Formulario</h1>
    <form action="procesar.php" method="post">
   
        <label for="nombre">Nombre:</label>
        <input type="text" id="nombre" name="nombre"><br>
 
        <label for="apellido">Apellido:</label>
        <input type="text" id="apellido" name="apellido"><br>

        <label for="cedula">Cédula:</label>
        <input type="number" id="cedula" name="cedula" min="0"><br>
  
        <label for="hobby">Hobby:</label>
        <input type="text" id="hobby" name="hobby"><br>
 
        <label>Sexo:</label><br>
        <input type="radio" name="sexo" value="masculino"> Masculino<br>
        <input type="radio" name="sexo" value="femenino"> Femenino<br>
 
        <label for="comentarios">Comentarios:</label><br>
        <textarea id="comentarios" name="comentarios" rows="4" cols="50"></textarea><br>

        <input type="submit" value="Enviar">
    </form>
</body>
</html>
