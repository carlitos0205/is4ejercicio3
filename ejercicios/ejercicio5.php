<?php
if ($_SERVER["REQUEST_METHOD"] === "POST") { 
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $edad = $_POST["edad"];
 
    echo <<<HTML
    <!DOCTYPE html>
    <html>
    <head>
        <title>Resultado del Formulario</title>
    </head>
    <body>
        <h1>Datos Ingresados:</h1>
        <p>Nombre: $nombre</p>
        <p>Apellido: $apellido</p>
        <p>Edad: $edad</p>
    </body>
    </html>
HTML;
} else {
    
    echo <<<HTML
    <!DOCTYPE html>
    <html>
    <head>
        <title>Formulario PHP</title>
    </head>
    <body>
        <h1>Formulario PHP</h1>
        <form method="POST" action="">
            <label for="nombre">Nombre:</label>
            <input type="text" name="nombre" required><br><br>
            
            <label for="apellido">Apellido:</label>
            <input type="text" name="apellido" required><br><br>
            
            <label for="edad">Edad:</label>
            <input type="number" name="edad" required><br><br>
            
            <input type="submit" value="Enviar">
        </form>
    </body>
    </html>
HTML;
}
?>
