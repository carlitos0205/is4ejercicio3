<?php
 
echo "Versión de PHP utilizada: " . PHP_VERSION . "<br>";
 
echo "ID de la versión de PHP: " . PHP_VERSION_ID . "<br>";
 
echo "Valor máximo soportado para enteros: " . PHP_INT_MAX . "<br>";
 
echo "Tamaño máximo del nombre de un archivo: " . PHP_MAXPATHLEN . "<br>";
 
echo "Versión del Sistema Operativo: " . PHP_OS . "<br>";
 
echo "Símbolo correcto de 'Fin De Línea': " . PHP_EOL . "<br>";
 
echo "Include path por defecto: " . get_include_path() . "<br>";
?>
