<!--13-Ejercicio 13
Crear un array asociativo de 10 elementos. Los valores de los 10 elemento del array deben ser
strings.
Crear un array asociativo de un elemento. El valor del array debe ser un string.
El script PHP debe hacer lo siguiente:
• Imprimir en pantalla si existe la clave del array de un elemento en el array de 10
elementos (si no existe, se imprime un mensaje).
• Imprimir en pantalla si existe el valor del vector de un elemento en el vector de 10
elementos (si no existe, se imprime un mensaje).
• Si no existe ni la clave ni el valor (del vector de un elemento) en el vector mayor se inserta
como un elemento más del vector.-->
<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
    <title>Ejercicio 13</title>
    </head>
    <body>

    <?php

    $vector = array( "cadena1" => "valor1", "cadena2" => "valor2", "cadena3" => "valor3", "cadena4" => "valor4",
                    "cadena5" => "valor5", "cadena6" => "valor6", "cadena7" => "valor7", "cadena8" => "valor8",
                    "cadena9" => "valor9", "cadena10" => "valor10"
                    );

    $buscado1 = array( "cad"     => "val1" );
    $buscado2 = array( "cadena1" => "val1" );
    $buscado3 = array( "cade"    => "valor1" );
    $buscado4 = array( "cadena1" => "valor1" );

    $indices = array_keys($vector);
    $valores = array_values($vector);

    $indiceBuscado = array_keys($buscado4);
    $valorBuscado = array_values($buscado4);

    $tamanho1 = count($indices);
    $tamanho2 = count($valores);

    $indiceEncontrado = false;
    $valorEncontrado  = false; 

    for( $i = 0; $i < $tamanho1; $i++ )
        if( $indices[$i] == $indiceBuscado[0] )
        $indiceEncontrado = true;
        
    for( $i=0; $i < $tamanho2; $i++ )
        if( $indices[$i] == $indiceBuscado[0] )
            $valorEncontrado = true;

    if( $indiceEncontrado )
        echo "Se encontro; el indice<br/>";
    else	
        echo "No se encontro; el indice<br/>";

    if( $valorEncontrado )
        echo "Se encontro; el valor<br/>";
    else
        echo "No se encontro; el valor<br/>";

    if( !$indiceEncontrado && !$valorEncontrado )
        $vector[$indiceBuscado] = $valorBuscado;

    echo "<br/><br/>";
    print_r($vector);
    ?>

    </body>
</html>