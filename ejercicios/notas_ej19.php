<?php

function abrirArchivo($path)
{
	if( !file_exists($path) )
	{
		echo 'El archivo no existe<br/>';
		return -1;
	}

	$manejador = fopen($path,"r+");

	if( !$manejador )
	{
		echo "El archivo no se pudo abrir por algún motivo<br/>";
		return -1;
	}

	return $manejador;
}

function cargarNotas($pathLectura,$pathEscritura)
{
	$manejadorLectura   = abrirArchivo($pathLectura);
	$manejadorEscritura = abrirArchivo($pathEscritura);

	if( $manejadorLectura == -1 )
		die('No se pudo abrir');

	if( $manejadorEscritura == -1 )
		die('No se pudo abrir');

	$calificaciones = file($pathLectura);
	
	foreach ( $calificaciones as $value ) 
	{
		list($matricula,$nombre,$apellido,$nota1,$nota2,$nota3) = explode(" ", $value);
		$notaFinal = (integer)$nota1 + (integer)$nota2 + (integer)$nota3;
		$cadena    = $matricula ." ". $notaFinal."\n";
		fwrite($manejadorEscritura, $cadena,strlen($cadena));
	}
	
	fclose($manejadorEscritura);
	fclose($manejadorLectura);
	echo "El archivo de suma de notas se generó con éxito.";
}
