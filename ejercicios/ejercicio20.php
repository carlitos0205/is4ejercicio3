<!-- Leer un archivo agenda.txt (creado por el alumno), este archivo contendrá el nombre y el apellido
de personas.
Se debe crear una función PHP que busque un nombre y un apellido en dicho archivo e imprima un
mensaje si se encontró o no se encontró el nombre y apellido en el archivo. Nombre y apellido son
variables que se debe introducir el usuario por formulario.
Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio. -->
<!DOCTYPE html>
<html>
<head>
    <title>Buscar en agenda</title>
</head>
<body>
    <h1>Buscar en la agenda</h1>
    <form action="buscar.php" method="post">
        <label>Nombre:</label>
        <input type="text" name="nombre" required><br>
        <label>Apellido:</label>
        <input type="text" name="apellido" required><br>
        <input type="submit" value="Buscar">
    </form>
</body>
</html>
