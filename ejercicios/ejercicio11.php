<!--11-Ejercicio 11:
Realizar un script PHP que haga lo siguiente:
Declare un vector indexado cuyos valores sean urls (esto simula un log de acceso de un usuario).
El script debe contar e imprimir cuantas veces accedió el usuario al mismo url (no se deben repetir
los url).
Obs: El alumno deberá crear sus propias funciones para realizar este ejercicio.-->
<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
    <title>Ejercicio 11</title>
    </head>
    <body>

    <?php

    $urls = array( "http://www.abc.com.py",  "http://www.abc.com.py",        "https://mail.google.com",
                "http://gitref.org",      "https://mail.google.com",      "http://aulavirtual.uc.edu.py",
                "http://www.hattick.org", "http://aulavirtual.uc.edu.py", "http://www.hattick.org",
                "http://www.abc.com.py" , "http://www.hattick.org",        "http://aulavirtual.uc.edu.py"
                );

    $bitacora = array();

    foreach ( $urls as $i => $v )
    {
        if( isset( $bitacora[$urls[$i]] ) )
            $bitacora[$urls[$i]]++;
        else
            $bitacora[$urls[$i]] = 1;
    }

    echo "El acceso del usuario fue el siguiente:<br/>";
    $tamanho = count($bitacora);

    foreach ($bitacora as $i=>$v)
        echo $i." accesos:".$v."<br/>";

    ?>

    </body>
</html>