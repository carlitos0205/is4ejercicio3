<!--9- Ejercicio 9:
Realizar un script en PHP que declare un vector de 100 elementos con valores aleatorios enteros
entre 1 y 100 y sume todos los valores. El script debe imprimir un mensaje con la sumatoria de los
elementos
Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio.-->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 9</title>
    </head>
    <body>

    <?php

    mt_srand(time());

    $vector = array();

    $sum = 0;

    for( $i = 0; $i < 100 ; $i++ )
    {
        $vector[] =  mt_rand(1,100);
        $sum      += $vector[$i];
    }

    echo "La suma de los elementos del vector es ". $sum;
    echo "<br/><br/>";
    print_r($vector);
    ?>

    </body>
</html>
