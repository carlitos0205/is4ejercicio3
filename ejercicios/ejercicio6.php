<!DOCTYPE html>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Ejercicio 6</title>
    <!-- 6- Ejercicio 6:
Hacer un script en PHP que simule una calculadora con las operaciones básicas: suma, resta,
multiplicación y división.
• El usuario debe introducir el operando 1.
• El usuario debe introducir el operando 2.
• El usuario debe seleccionar la operación que quiere realizar (a través de un select).
• El usuario debe presionar el botón calcular.
• El script debe calcular el resultado de la operación e imprimirlo en pantalla. El script debe
realizar los controles respectivos (para casos donde las operaciones no son válidas).
-->
    </head>
    <body>
    <?php

    /*Cadena Heredoc, permite expandir variables en PHP*/
    $str=<<<HTML
        <form action="#" method="post">
            <div>
                <label for="operando1">Operando 1:</label>
                <input type="text" name="operando1" placeholder="Introduzca el operando1" />
            </div>
            <br/>
            <div>
                <label for="operando2">Operando 2:</label>
                <input type="text" name="operando2" placeholder="Introduzca el operando2" />
            </div>
            <br/>
            <div>
                <label>Operacion:&nbsp;&nbsp;</label>
                <select name="operacion">
                <option value="s">Suma</option>
                <option value="r">Resta</option>
                <option value="m">Multiplicacion</option>
                <option value="d">Division</option>
                </select>
            </div>
            <br/>
            <div class="button">
                <button type="submit">Calcular</button>
            </div>
        </form>
    HTML;



    if( !isset($_POST['operando1']) && !isset($_POST['operando2']) && !isset($_POST['operacion']) )
    {

        echo $str; //Imprimo el formulario cuando no me llega información por Post

    }else{
        
        $operando1 = $_POST['operando1'];
        $operando2 = $_POST['operando2'];
        $operacion = $_POST['operacion']; 	
        
        echo "$operando1 ";
        $resultado = generarResultado($operando1,$operando2,$operacion);
        echo "$operando2 = ";
        echo "<b>$resultado</b>";
        echo "<br/><br/><a href='practica1.php'>Volver a Calculadora</a>";
        
    }

    function generarResultado($operando1,$operando2,$operacion)
    {
        if($operacion=='s')
        {
            echo " + ";
            $resultado=$operando1+$operando2;

        }
        elseif ( $operacion == 'r' ) {
            
            echo " - ";
            $resultado = $operando1 - $operando2;
        
        }elseif ( $operacion == 'm' ) {
            
            echo " * ";
            $resultado = $operando1 * $operando2;
        
        }else{

            echo " / ";
            if( $operando2 == 0 )
                $resultado = "No es valida la operacion, no se puede dividir por cero.";	
            else	
                $resultado = $operando1 / $operando2;
        }

        return $resultado;
    }

?>
</body>
</html>