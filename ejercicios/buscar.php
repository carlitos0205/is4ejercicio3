<?php
// Función para buscar el nombre y apellido en el archivo "agenda.txt"
function buscarEnAgenda($nombre, $apellido) {
    $archivo = fopen("agenda.txt", "r");
    if ($archivo) {
        while (($linea = fgets($archivo)) !== false) {
            // Elimina espacios en blanco y saltos de línea al principio y al final de la línea
            $linea = trim($linea);
            // Divide la línea en nombre y apellido (suponiendo que están separados por un espacio en blanco)
            $datos = explode(" ", $linea);
            $nombreEnArchivo = $datos[0];
            $apellidoEnArchivo = $datos[1];
            // Compara el nombre y apellido con los proporcionados por el usuario
            if ($nombreEnArchivo == $nombre && $apellidoEnArchivo == $apellido) {
                fclose($archivo);
                return true; // Se encontró la coincidencia
            }
        }
        fclose($archivo);
    }
    return false; // No se encontró la coincidencia
}

// Obtener los valores del formulario
$nombreBuscado = $_POST["nombre"];
$apellidoBuscado = $_POST["apellido"];

// Realizar la búsqueda
if (buscarEnAgenda($nombreBuscado, $apellidoBuscado)) {
    echo "Se encontró el nombre y apellido en la agenda.";
} else {
    echo "No se encontró el nombre y apellido en la agenda.";
}
?>
