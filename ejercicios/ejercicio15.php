<!--15- Ejercicio 15
Declarar un vector de 20 elementos con valores aleatorios de 1 a 10.
Crear una función recursiva en PHP que recorra el array de atrás para adelante. Se deberá
imprimir desde el último elemento del array hasta el primero
Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio.-->
<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
    <title>Ejercicio 15</title>
    </head>
    <body>
    <?php

    $v = crearVector();
    imprimirSinRecursion($v);
    echo " (Tamanho del vector ".count($v).")<br/>";
    imprimirRecursivo($v,count($v));
    echo " (Tamanho del vector ".count($v).")<br/>";

    function crearVector()
    {
        mt_srand(time());
        
        for ( $i = 0 ; $i < 20 ; $i++ )
            $vector[] = mt_rand(1,10);
        
        return $vector;
    }

    function imprimirSinRecursion($vector)
    {
        foreach ( $vector as $i => $v )
            echo $v." ";
    }

    function imprimirRecursivo($vector,$i)
    {
        if( $i == 0 ) //Caso Base
            return;
        
        echo $vector[$i-1]." ";
        
        imprimirRecursivo($vector, $i-1);
    }


    ?>
    
    </body>
</html>