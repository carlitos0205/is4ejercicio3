<!--10-Ejercicio 10:
Hacer un script PHP que declare un vector de 50 elementos con valores aleatorios enteros entre 1
y 1000. El script debe determinar cuál es el elemento con mayor valor en el vector, se debe
imprimir un mensaje con el valor y el índice en donde se encuentra.
Mensaje de ejemplo: El elemento con índice 8 posee el mayor valor que 789.
Obs: El alumno deberá crear sus propias funciones para realizar este ejercicio.-->
<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
    <title>Ejercicio 10</title>
    </head>
    <body>

    <?php

    mt_srand(time());

    $vector = array();
    $mayor  = 0;
    $indice = -1;

    for( $i = 0; $i < 50 ; $i++ )
    {
        $vector[] =  mt_rand(1,100);
        if( $vector[$i] > $mayor )
        {
            $mayor  = $vector[$i];
            $indice = $i;
        }
    }

    echo "El elemento con indice $indice posee el mayor valor que es igual a $mayor";
    echo "<br/><br/>";
    print_r($vector);
    ?>

    </body>
</html>
