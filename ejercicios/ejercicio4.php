<!DOCTYPE html>
<html>
<head>
    <title>Ordenar Números Aleatorios</title>
</head>
<body>
<?php 
mt_srand(); 
$numero1 = mt_rand(50, 900);
$numero2 = mt_rand(50, 900);
$numero3 = mt_rand(50, 900); 

$mayor = max($numero1, $numero2, $numero3);
$menor = min($numero1, $numero2, $numero3);
$intermedio = ($numero1 + $numero2 + $numero3) - ($mayor + $menor);
 
echo "Números ordenados: ";
if ($mayor === $numero1) {
    echo "<span style='color: green;'>$mayor</span> ";
} else {
    echo "$mayor ";
}
if ($intermedio === $numero2) {
    echo "<span style='color: black;'>$intermedio</span> ";
} else {
    echo "$intermedio ";
}
if ($menor === $numero3) {
    echo "<span style='color: red;'>$menor</span>";
} else {
    echo "$menor";
}
?>
</body>
</html>
