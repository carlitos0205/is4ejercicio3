<!--12- Ejercicio 12
Hacer un script PHP que haga lo siguiente:
• Crear un array indexado con 10 cadenas.
• Crear una variable que tenga un string cualquiera como string.
El script debe:
• Buscar si la cadena declarada está en el vector declarado.
• Si esta, se imprime “Ya existe” la cadena.
• Si no está, se imprime “Es Nuevo” y se agrega al Final del array.
Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio.-->
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ejercicio 12</title>
</head>
<body>

<?php

$cadenas = array( "cadena1","cadena2","cadena3","cadena4","cadena5","cadena6","cadena7",
		          "cadena8","cadena9","repetida"
                );

$cadena1 = "repetida";
$cadena2 = "cadena nueva";

if( in_array($cadena1,$cadenas) )
	echo "Existe la cadena<br/>";
else{
		echo "Es nueva<br/>";
		$cadenas[] = $cadena1;
	}

var_dump($cadenas);
echo "<br/><br/>";

if( in_array($cadena2,$cadenas) )
	echo "Existe la cadena<br/>";
else{
		echo "Es nueva<br/>";
		$cadenas[] = $cadena2;
	}
var_dump($cadenas);
echo "<br/><br/>";
?>

</body>
</html>